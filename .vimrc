set nocompatible              " be iMproved, required
filetype off                  " required

set rtp+=~/.vim/bundle/Vundle.vim

call vundle#begin()

Plugin 'VundleVim/Vundle.vim'

Plugin 'valloric/youcompleteme'
"let g:ycm_path_to_python_interpreter = "'/Users/shaun.gamboa/.pyenv/shims/python' 

" Plugin 'luochen1990/rainbow'
" let g:rainbow_active = 1 "set to 0 if you want to enable it later via
" :RainbowToggle

 Plugin 'jiangmiao/auto-pairs'

 Plugin 'scrooloose/syntastic'
 set statusline+=%#warningmsg#
 set statusline+=%{SyntasticStatuslineFlag()}
 set statusline+=%*

 let g:syntastic_always_populate_loc_list = 1
 let g:syntastic_auto_loc_list = 1
 let g:syntastic_check_on_open = 1
 let g:syntastic_check_on_wq = 0


 Plugin 'lifepillar/vim-solarized8'
 let g:solarized_visibility = "high"
 let g:solarized_diffmode = "high"

" "Plugin 'tpope/vim-fugitive'

 Plugin 'junegunn/goyo.vim'
 Plugin 'reedes/vim-colors-pencil'


 call vundle#end()            " required
 filetype plugin indent on    " require
 syntax on

 set background=dark
 colorscheme solarized8

 set number

 set t_Co=256

 set splitbelow
 set splitright


 function! s:goyo_enter()
     colorscheme pencil
 endfunction

     function! s:goyo_leave()
         colorscheme solarized8
     endfunction

         autocmd! User GoyoEnter nested call <SID>goyo_enter()
         autocmd! User GoyoLeave nested call <SID>goyo_leave()

